// init const draggables, qui cible de la classe dragboxes (toutes nos divs de classe .dragboxes qu'on va déplacer)
const draggables =  document.querySelectorAll('.dragboxes')
// on va cibler ici notre milieu de page ou les éléments vont pouvoir se déplacer (dans la div de class .middlebigbox du coup)
const bigboxright = document.querySelectorAll('.bigboxright')
const leftupbox = document.querySelectorAll('.leftupbox')
const middleleftbox = document.querySelectorAll('.middleleftbox')
const bottomleftbox = document.querySelectorAll('.bottomleftbox')


// on va rentrer dans une loop pour chacune de nos dragboxes lorsque  on va en drag une
draggables.forEach(dragboxes => {
    dragboxes.addEventListener('dragstart',() => { // début du drag => loop
        console.log('dragstart'); // ici on peut l'observer dans la console
        dragboxes.classList.add('dragging') // pour add la class qui ajoute de l'opacité lors d'un drag
    })

    dragboxes.addEventListener('dragend', () => { // fin du drag => loop
        dragboxes.classList.remove('dragging') // pour suppr la class dragging de dragboxes (remove l'opacité une fois la dragboxes lachée)
    })
})

leftupbox.forEach(leftupbox => { //  on cible notre div
    leftupbox.addEventListener('dragover', () => {  // on créé un event si on survol une div lors d'un drag d'une dragboxes
        console.log('dragover'); // on peut le voir ici dans la console 
        
        const dragboxes  = document.querySelector('.dragging')
        leftupbox.appendChild(dragboxes)
    })
})

middleleftbox.forEach(middleleftbox => { //  on cible notre div
    middleleftbox.addEventListener('dragover', () => {  // on créé un event si on survol une div lors d'un drag d'une dragboxes
        console.log('dragover'); // on peut le voir ici dans la console 
       
        const dragboxes  = document.querySelector('.dragging')
        middleleftbox.appendChild(dragboxes)
    })
})

bottomleftbox.forEach(bottomleftbox => { //  on cible notre div
    bottomleftbox.addEventListener('dragover', () => {  // on créé un event si on survol une div lors d'un drag d'une dragboxes
        console.log('dragover'); // on peut le voir ici dans la console 
       
        const dragboxes  = document.querySelector('.dragging')
        bottomleftbox.appendChild(dragboxes)
    })
})

//___________________________________________________________________________________________________________________________________________________________
// sert à montrer que le dépot des dragboxes et possible au survol des box par les dragboxes
leftupbox.forEach(leftupbox =>  {
    leftupbox.addEventListener('dragover', e => {
        e.preventDefault()
        const dragboxes = document.querySelector('.dragging')
        leftupbox.appendChild(dragboxes)
    })
})

middleleftbox.forEach(middleleftbox =>  {
    middleleftbox.addEventListener('dragover', e => {
        e.preventDefault()
        const dragboxes = document.querySelector('.dragging')
        middleleftbox.appendChild(dragboxes)
    })
})

bottomleftbox.forEach(bottomleftbox =>  {
    bottomleftbox.addEventListener('dragover', e => {
        e.preventDefault()
        const dragboxes = document.querySelector('.dragging')
        bottomleftbox.appendChild(dragboxes)
    })
})

bigboxright.forEach(bigboxright =>  {
    bigboxright.addEventListener('dragover', e => {
        e.preventDefault()
        const dragboxes = document.querySelector('.dragging')
        bigboxright.appendChild(dragboxes)
    })
})
//___________________________________________________________________________________________________________________________________________________________
